#!/bin/bash

## Create groups and users

sudo adduser --disabled-password --ingroup ubuntu admin;
sudo addgroup groupe_a && sudo adduser --no-create-home --disabled-password --ingroup groupe_a lambda_a;
sudo addgroup groupe_b && sudo adduser --no-create-home --disabled-password --ingroup groupe_b lambda_b;

## Create folders and set permissions

sudo mkdir dir_a dir_b dir_c \
&& (sudo chown admin:groupe_a dir_a; sudo chown admin:groupe_b dir_b; sudo chown admin: dir_c) \
&& (sudo -u admin chmod ug+rwx,o-rwx,g+s,+t dir_a dir_b; sudo -u admin chmod a+rwx dir_c);



##


sudo -u admin bash -c 'touch /home/admin/passwd' \
&& sudo -u admin bash -c 'chmod o-rwx /home/admin/passwd';

make && sudo chown root: group_server && sudo chmod +x group_server \
&& sudo chown admin: group_client && sudo chmod +x group_client;

sudo -u lambda_a bash -c 'echo -e "Ceci un texte écrit par lambda_a" > dir_a/lambda_a1.txt';
sudo -u lambda_b bash -c 'echo -e "Ceci un texte écrit par lambda_b" > dir_b/lambda_b1.txt';