#if !defined(UTILS)
#define UTILS

#define SOCKET int
#define SOCKADDR_IN struct sockaddr_in
#define SOCKADDR struct sockaddr
#define IN_ADDR struct in_addr

#define INVALID_SOCKET -1
#define SOCKET_ERROR -1
#define PORT 4000
#define BUFFER_SIZE 1024
#define PWD_SIZE 32

#endif // UTILS
