#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include "utils.h"
#include "check_pass.h"

int handle_client(SOCKET client_sock, uid_t euid, gid_t egid)
{

    int n = 0, i = 0;
    char buffer[BUFFER_SIZE], delim[] = " ", *left, *right, command[BUFFER_SIZE];
    FILE *f;

    if ((n = recv(client_sock, buffer, BUFFER_SIZE - 1, 0)) < 0)
    {
        perror("recv()");
        exit(errno);
    }
    buffer[n] = '\0';
    left = strtok(buffer, delim);
    right = strtok(NULL, delim);

    if (!check_pass("/home/admin/passwd", atoi(left), right))
    {
        perror("Wrong password, please try again !\n");
        exit(EXIT_FAILURE);
    }
    char *message = "Authentification succeed !";
    if (send(client_sock, message, strlen(message), 0) < 0)
    {
        perror("send()");
        exit(errno);
    }

    seteuid(euid);
    setegid(egid);

    do
    {
        if ((n = recv(client_sock, buffer, BUFFER_SIZE - 1, 0)) < 0)
        {
            perror("recv()");
            exit(errno);
        }
        buffer[n] = '\0';
        //printf("%d: %s\n", i, buffer);
        left = strtok(buffer, delim);
        right = strtok(NULL, delim);

        //printf("%s %s\n", left, right);
        if (!strcmp(left, "close"))
        {
            char *message = "\nSession terminée !";
            if (send(client_sock, message, strlen(message), 0) < 0)
            {
                perror("send()");
                exit(errno);
            }
            printf("%d: %s\n", i, left);
        }
        else if (left && right) // executer les commandes
        {
            if (!strcmp(left, "list"))
                strcpy(command, "ls -al ");
            else if (!strcmp(left, "read"))
                strcpy(command, "cat ");
            else
                exit(EXIT_FAILURE);

            strcat(command, right);

            printf("%d: %s\n", i, command);

            if ((f = popen(command, "r")) == NULL)
                exit(1);

            if ((fread(buffer, 1, BUFFER_SIZE, f)) > 0)
            {
                if (send(client_sock, buffer, strlen(buffer), 0) < 0)
                {
                    perror("send()");
                    exit(errno);
                }
                printf("%s\n", buffer);
            }
            pclose(f);
        }
        i++;
    } while (left && right);

    return 0;
}

int main(int argc, char const *argv[])
{
    char buffer[BUFFER_SIZE], delim[] = " ", *euid, *egid;
    int n = 0;
    SOCKET sock = socket(AF_INET, SOCK_STREAM, 0);
    if (sock == INVALID_SOCKET)
    {
        perror("socket()");
        exit(errno);
    }

    SOCKADDR_IN sin = {0};
    sin.sin_addr.s_addr = htonl(INADDR_ANY); /* nous sommes un serveur, nous acceptons n'importe quelle adresse */
    sin.sin_family = AF_INET;
    sin.sin_port = htons(PORT);
    if (bind(sock, (SOCKADDR *)&sin, sizeof sin) == SOCKET_ERROR)
    {
        perror("bind()");
        exit(errno);
    }

    if (listen(sock, 5) == SOCKET_ERROR)
    {
        perror("listen()");
        exit(errno);
    }

    while (1)
    {
        SOCKADDR_IN csin = {0};
        socklen_t sinsize = sizeof csin;
        SOCKET csock = accept(sock, (SOCKADDR *)&csin, &sinsize);
        if (csock == INVALID_SOCKET)
        {
            perror("accept()");
            exit(errno);
        }

        printf("\nClient connected !\n");

        if ((n = recv(csock, buffer, BUFFER_SIZE - 1, 0)) < 0)
        {
            perror("recv()");
            exit(errno);
        }
        buffer[n] = '\0';
        //printf("%d: %s\n", i, buffer);
        euid = strtok(buffer, delim);
        egid = strtok(NULL, delim);

        //printf("==>%s, %s\n", euid, egid);

        handle_client(csock, atoi(euid), atoi(egid)); // Gerer la connection d'un utilisateur ici
        close(csock);
    }

    close(sock);
    return 0;
}
