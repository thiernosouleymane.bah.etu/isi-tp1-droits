#include <stdio.h>
#include <stdlib.h>
#include <netdb.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include "utils.h"

int main(int argc, char const *argv[])
{

    if (argc != 2)
    {
        fprintf(stderr, "Usage : %s <fichier>\n", argv[0]);
        fprintf(stderr, "<fichier> : Fichier contenant les instructions\n");
    }

    SOCKET sock = socket(AF_INET, SOCK_STREAM, 0);
    if (sock == INVALID_SOCKET)
    {
        perror("socket()");
        exit(errno);
    }

    struct hostent *hostinfo = NULL;
    SOCKADDR_IN sin = {0}; /* initialise la structure avec des 0 */
    const char *hostname = "127.0.0.1";

    hostinfo = gethostbyname(hostname); /* on récupère les informations de l'hôte auquel on veut se connecter */
    if (hostinfo == NULL)               /* l'hôte n'existe pas */
    {
        fprintf(stderr, "Unknown host %s.\n", hostname);
        exit(errno);
    }

    sin.sin_addr = *(IN_ADDR *)hostinfo->h_addr; /* l'adresse se trouve dans le champ h_addr de la structure hostinfo */
    sin.sin_port = htons(PORT);                  /* on utilise htons pour le port */
    sin.sin_family = AF_INET;
    if (connect(sock, (SOCKADDR *)&sin, sizeof(SOCKADDR)) == SOCKET_ERROR)
    {
        perror("connect()");
        exit(errno);
    }

    FILE *f;
    char *line = NULL;
    size_t len = 0;
    int n = 0, i = 0;
    char buffer[BUFFER_SIZE];

    // sprintf(buffer, "%d %d ", geteuid(), getegid());
    // printf("=>>%s\n", buffer);

    if (send(sock, buffer, strlen(buffer), 0) < 0)
    {
        perror("send()");
        exit(errno);
    }

    if ((f = fopen(argv[1], "r")) != NULL)
    {
        while (getline(&line, &len, f) != -1)
        {
            if (i != 0)
                printf("\nubuntu@instance-1:~$ %s", line);
            if (send(sock, line, len, 0) < 0)
            {
                perror("send()");
                exit(errno);
            }

            if ((n = recv(sock, buffer, BUFFER_SIZE - 1, 0)) < 0)
            {
                perror("recv()");
                exit(errno);
            }
            buffer[n] = '\0';
            printf("%s\n", buffer);
            i++;
        }
        free(line);
        fclose(f);
    }

    close(sock);
    return 0;
}
