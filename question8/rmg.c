#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include "check_pass.h"

#define MAX_SIZE 32

int main(int argc, char const *argv[])
{
    uid_t *ruid, *euid, *suid;
    struct stat sb;
    gid_t gid;
    uid_t uid;
    char password[MAX_SIZE], i = 0, c;

    if (argc != 2)
    {
        fprintf(stderr, "Usage : %s <fichier>\n", argv[0]);
        fprintf(stderr, "<fichier> : Nom de fichier à effacer\n");
    }

    gid = getgid();
    uid = getuid();
    stat(argv[1], &sb);

    if (gid != sb.st_gid)
    {
        fprintf(stderr, "Vous n'avez le droit de modifier le fichier %s\n", argv[1]);
        return EXIT_FAILURE;
    }

    printf("Veuillez saisir votre mot de passe: ");
    //sscanf("%32s", password);
    fgets(password, MAX_SIZE, stdin);
    printf("%s\n", password);

    //printf("up> %d, %s\n", uid, password);
    if (check_pass("/home/admin/passwd", uid, password))
    {
        int del = remove(argv[1]);
        if (!del)
            printf("%s supprimé avec succès\n", argv[1]);
        else
            printf("La suppression du fichier %s a échoué\n", argv[1]);
    }
    else
    {
        printf("Mot de passe incorrect\n");
        return 0;
    }

    return 1;
}
