#!/bin/bash

## lamba_a tests

echo -e "> lambda_a essaie de supprimer un fichier dans le repertoire dir_b" \
&& sudo -u lambda_a bash -c 'echo "lambda" | ./rmg dir_b/lambda_b1.txt';

echo -e "\n> lambda_a essaie de supprimer un fichier dans le repertoire dir_a avec un mot de passe incorrect" \
&& sudo -u lambda_a bash -c 'echo "wrong" | ./rmg dir_a/lambda_a1.txt';

echo -e "\n> lambda_a essaie de supprimer un fichier dans le repertoire dir_a avec un mot de passe correct" \
&& sudo -u lambda_a bash -c 'echo "lambda" | ./rmg dir_a/lambda_a1.txt';

## lambda_b tests

echo -e "\n\n> lambda_b essaie de supprimer un fichier dans le repertoire dir_a" \
&& sudo -u lambda_b bash -c 'echo "lambda" | ./rmg dir_a/lambda_a1.txt';

echo -e "\n> lambda_b essaie de supprimer un fichier dans le repertoire dir_b avec un mot de passe incorrect" \
&& sudo -u lambda_b bash -c 'echo "wrong" | ./rmg dir_b/lambda_b1.txt';

echo -e "\n> lambda_b essaie de supprimer un fichier dans le repertoire dir_b avec un mot de passe correct" \
&& sudo -u lambda_b bash -c 'echo "lambda" | ./rmg dir_b/lambda_b1.txt';
