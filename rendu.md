# Rendu "Les droits d’accès dans les systèmes UNIX"

## Binome

- Nom, Prénom, email: BAH, Thierno-Souleymane, thiernosouleymane.bah.etu@univ-lille.fr

- Nom, Prénom, email: BALLOT, Raoul, raoul.ballot.etu@univ-lille.fr

## Question 1

Le groupe **ubuntu** existant déjà, nous n'avons pas à le recréer.

```bash
ubuntu@instance-1:~$ sudo adduser toto # Création de l'utilisateur toto
ubuntu@instance-1:~$ sudo adduser toto ubuntu # Ajout au groupe ubuntu
```

toto ne peut pas ouvrir en écriture le fichier **myfile.txt** car étant proprietaire du fichier, il a comme permission **r--**, il ne peut qu'ouvrir le fichier en lecture.

## Question 2

- Le caractère **x** indique, pour un repertoire, qu'on a le droit de l'ouvrir (d'acceder à tous les fichiers qu'il contient).

- Le dossier **mydir** étant inaccessible au groupe **ubuntu** et sachant que toto fait partie du groupe ubuntu alors toto ne peut pas accéder au dossier **mydir**.

  ```bash
  ubuntu@instance-1:~$ mkdir mydir && chmod g-x mydir # Création du dossier mydir et retrait du droit x à ubuntu
  toto@instance-1:/home/ubuntu$ cd mydir
  bash: cd: mydir: Permission denied
  ```

- L'utilisateur toto n'ayant pas accès au dossier **mydir**, n'a donc pas le droit de lister le contenu de celui-ci.

  ```bash
  ubuntu@instance-1:~$ touch mydir/data.txt && echo "Lorem ipsum dolor sit amet, consectetur adipiscing elit." > mydir/data.txt
  toto@instance-1:/home/ubuntu$ ls -al mydir/
  ls: cannot access mydir/.: Permission denied
  ls: cannot access mydir/..: Permission denied
  ls: cannot access mydir/data.txt: Permission denied
  total 0
  d????????? ? ? ? ?            ? .
  d????????? ? ? ? ?            ? ..
  -????????? ? ? ? ?            ? data.txt
  ```

## Question 3

- Non, le processus n'arrive pas ouvrir le fichier data.txt en lecture

  ```bash
  toto@instance-1:/home/ubuntu$ ./suid
  EUID: 1001
  EGID: 1001
  RUID: 1001
  RGID: 1001

  Cannot open file
  ```

- Oui, le processus arrive ouvrir le fichier data.txt en lecture car le EUID vaut 1000 qui est celui de l'utilisateur ubuntu.

  ```bash
  ubuntu@instance-1:~$ chmod u+s suid
  toto@instance-1:/home/ubuntu$ ./suid
  EUID: 1000
  EGID: 1001
  RUID: 1001
  RGID: 1001

  Lorem ipsum dolor sit amet, consectetur adipiscing elit.
  ```

## Question 4

```bash
ubuntu@instance-1:~$ ./suid.py
EUID:  1000
EGID:  1000
toto@instance-1:/home/ubuntu$ ./suid.py
EUID:  1001
EGID:  1001
```

- On ne constate aucun changement.
- Un utilisateur pourrait modifier le fichier passwd (sans demander à un administrateur) en utilisant un script avec le set-user-id d'activé.

## Question 5

- La commande **chfn** permet de modifier le nom complet et les informations associées à un utilisateur.

  ```bash
  ubuntu@instance-1:~$ ls -al /usr/bin/chfn
  -rwsr-xr-x 1 root root 85064 May 28  2020 /usr/bin/chfn
  ```

  Les utilisateurs appartenant au groupe **root** ont le droit d'exécuter la commande avec l'EUID de root. Celà permet aux utilisateurs de pouvoir modifier (par procuration) certains fichiers tel que **/etc/passwd**.

  L'utilisateur est le seul autorisé à modifier /usr/bin/chfn, tous les utilisateurs peuvent le lire et l'exécuter.

- Les informations ont bien été mis à jour et sont corrects
  ```bash
  ubuntu@instance-1:~$ su toto
  toto@instance-1:/home/ubuntu$ chfn
  Password:
  Changing the user information for toto
  Enter the new value, or press ENTER for the default
          Full Name:
          Room Number []: Room 1
          Work Phone []: 123456789
          Home Phone []: 987654321
  toto@instance-1:/home/ubuntu$ cat /etc/passwd
  ...
  toto:x:1001:1001:,Room 1,123456789,987654321:/home/toto:/bin/bash
  ```

## Question 6

Les mots de passes sont chiffrés et stockés dans le fichier **/etc/shadow**.
C'est un fichier texte et lisible uniquement par l'utilisateur root et présente donc moins de risque de sécurité.

## Question 7

### Mise en place

Nous obtenons à la fin le resultat suivant:

```bash
ubuntu@instance-1:~$ ls -al
...
drwxrws--T 3 admin  groupe_a 4096 Jan 18 15:44 dir_a
drwxrws--T 2 admin  groupe_b 4096 Jan 18 15:43 dir_b
drwxrwxr-x 2 admin  admin    4096 Jan 18 15:43 dir_c
```

### Tests

Nous avons écrit 5 scripts pour mieux prendre en main cette section.

- [init.sh](question7/init.sh): permet de créer les groupes, utilisateurs et repertoires avec leur permission.
- [clean.sh](question7/clean.sh): permet de supprimer les groupes, utilisateurs et repertoires créés avec [init.sh](question7/init.sh).
- [admin.sh](question7/admin.sh) permet de valider les règles d'accessibiltés de l'utilisateur admin.
- [lambda_a.sh](question7/lambda_a.sh) permet de valider les règles d'accessibiltés de l'utilisateur lambda_a.
- [lambda_b.sh](question7/lambda_b.sh) permet de valider les règles d'accessibiltés de l'utilisateur lambda_b.

> Nous vous conseillons de lancer le script [admin.sh](question7/admin.sh) avant les scripts [lambda_a.sh](question7/lambda_a.sh) et [lambda_b.sh](question7/lambda_b.sh) car pour éviter de les recréé, nous utilisons les fichiers crées par admin dans les autres tests.

## Question 8

Nous avons écrit 3 scripts pour mieux prendre en main cette section.

- [init.sh](question8/init.sh): permet de créer les groupes, utilisateurs, repertoires avec leur permission, le fichier passwd et de compiler le programme rmg.
- [clean.sh](question8/clean.sh): permet de supprimer les groupes, utilisateurs, repertoires et les fichiers créés avec [init.sh](question8/init.sh).
- [test.sh](question8/test.sh) permet de tester le programme rmg.

Toutefois, voici un apercu de ce qu'on a fait.

```bash
ubuntu@instance-1:~$ ls -al rmg
-rwsrwxr-x 1 admin ubuntu 17608 Jan 18 14:29 rmg
```

```bash
ubuntu@instance-1:~$ ls -al /home/admin/passwd
-rw-r----- 1 admin ubuntu 38 Jan 18 14:29 /home/admin/passwd
```

> Nous vous conseillons d'exécuter les scripts dans l'ordre suivant: [clean.sh](question8/clean.sh), [init.sh](question8/init.sh), [test.sh](question8/test.sh). <br/> Partir d'un environnement completement vierge serait plus simple pour le test.

## Question 9

Nous avons écrit 3 scripts pour mieux prendre en main cette section.

- [init.sh](question9/init.sh): permet de créer les groupes, utilisateurs, le fichier passwd et de compiler le programme pwg.
- [clean.sh](question9/clean.sh): permet de supprimer les groupes, utilisateurs et les fichiers créés avec [init.sh](question9/init.sh).
- [test.sh](question9/test.sh) permet de tester le programme pwg.

Toutefois, voici un apercu de ce qu'on a fait.

```bash
ubuntu@instance-1:~$ ls -al pwg
-rwsrwxr-x 1 admin ubuntu 17432 Jan 25 02:55 pwg
```

> A l'instar de la question 8, nous vous conseillons d'exécuter les scripts dans l'ordre suivant: [clean.sh](question9/clean.sh), [init.sh](question9/init.sh), [test.sh](question9/test.sh).

## Question 10

Nous avons écrit 2 scripts pour mieux prendre en main cette section.

- [init.sh](question10/init.sh): permet de créer les groupes, utilisateurs, repertoires avec leur permission, le fichier passwd et de compiler les programmes group_server et group_client.
- [clean.sh](question10/clean.sh): permet de supprimer les groupes, utilisateurs et les fichiers créés avec [init.sh](question10/init.sh).

Voici comment tester nos programmes:

- Executer **group_server** en root

```bash
ubuntu@instance-1:~$ sudo './group_server'
```

- Exécuter ensuite **group_client**, à partir d'un utilisateur (lambda_a ou lambda_b), avec un fichier qui contient les commandes

```bash
ubuntu@instance-1:~$ sudo -u lambda_a bash -c './group_client <command_file>'
```

> Nous vous conseillons de creer des mots de passes de l'utilisateur avec leque vous testez avec le programme de la question 9. Aussi des fichiers ont été créés pour vous faciliter les tests: dir_a/lambda_a1.txt et dir_b/lambda_b1.txt.
