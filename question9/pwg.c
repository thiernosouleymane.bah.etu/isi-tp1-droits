#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pwd.h>

#define MAX_SIZE 32

int main(int argc, char const *argv[])
{
    FILE *f_src, *f_dst;
    char *l_src = NULL, *l_dst = NULL, delim[] = ":", *pwd, *uid, not_found = 1;
    size_t len = 0;
    ssize_t size;
    char new_password[MAX_SIZE], old_password[MAX_SIZE], *crypt_passwd;

    uid_t user_id = getuid();

    // Check if user has already a password
    if ((f_src = fopen("/home/admin/passwd", "r")) != NULL)
    {
        while (not_found && (size = getline(&l_src, &len, f_src)) != -1)
        {
            uid = strtok(l_src, delim);
            pwd = strtok(NULL, delim);
            //printf("==> %d, %d, %s, %s\n", atoi(uid), user_id, password, pwd);
            if (atoi(uid) == user_id)
                not_found = 0;
        }
        fclose(f_src);
    }

    // The user has already a password
    if (size != -1)
    {
        printf("Veuillez saisir votre mot de passe: ");
        fgets(old_password, MAX_SIZE, stdin);
        printf("%s\n", old_password);

        if (strcmp(pwd, crypt(old_password, "salt")))
        {
            fprintf(stderr, "Mot de passe incorrect\n");
            free(l_src);
            return EXIT_FAILURE;
        }
        printf("Veuillez saisir le nouveau mot de passe: ");
    }
    else
    {
        printf("Veuillez saisir le mot de passe: ");
    }

    fgets(new_password, MAX_SIZE, stdin);
    printf("%s\n", new_password);

    // Write the new password
    if ((f_src = fopen("/home/admin/passwd", "r")) != NULL && (f_dst = fopen("/home/admin/passwd.tmp", "a")) != NULL)
    {
        while (getline(&l_dst, &len, f_src) != -1)
        {
            char *a = strtok(l_dst, delim);
            char *b = strtok(NULL, delim);

            if (atoi(a) != user_id)
                fprintf(f_dst, "%s:%s:\n", a, b);
        }
        fprintf(f_dst, "%d:%s:\n", user_id, crypt(new_password, "salt"));
        free(l_dst);
        fclose(f_src);
        fclose(f_dst);
    }

    printf("\n");
    remove("/home/admin/passwd");
    rename("/home/admin/passwd.tmp", "/home/admin/passwd");

    return EXIT_SUCCESS;
}
