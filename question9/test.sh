#!/bin/bash

### pwg

# L'utilisateur se crée un mdp
# L'utilisateur essaie de modifier son mot de passe avec un mauvais mot de passe
# L'utilisateur essaie de modifier son mot de passe avec le bon mot de passe

### rmg

# L'utilisateur tente de supprimer un fichier avec un mauvais mot de passe.
# L'utilisateur tente de supprimer un fichier avec le bon mot de passe.

### TESTS lambda_a

echo -e "> lambda_a tente de se créer un mot de passe avec succès" \
&& sudo -u lambda_a bash -c 'echo -e "lambda_a" | ./pwg';

echo -e "\n> lambda_a essaie de modifier son mot de passe avec un mauvais ancien mot de passe" \
&& sudo -u lambda_a bash -c 'echo -e "wrong\nnewpwd" | ./pwg';

echo -e "\n> lambda_a essaie de modifier son mot de passe avec le bon ancien mot de passe" \
&& sudo -u lambda_a bash -c 'echo -e "lambda_a\nnewpwd" | ./pwg';


### TESTS lambda_b

echo -e "\n\n> lambda_b tente de se créer un mot de passe avec succès" \
&& sudo -u lambda_b bash -c 'echo -e "lambda_b" | ./pwg';

echo -e "\n> lambda_b essaie de modifier son mot de passe avec un mauvais ancien mot de passe" \
&& sudo -u lambda_b bash -c 'echo -e "wrong\nnewpwd" | ./pwg';

echo -e "\n> lambda_b essaie de modifier son mot de passe avec le bon ancien mot de passe" \
&& sudo -u lambda_b bash -c 'echo -e "lambda_b\nnewpwd" | ./pwg';