#!/bin/bash

### Test de l'utilisateur lambda_b

sudo -u lambda_b bash -c 'cat dir_b/admin1.txt > /dev/null' \
&& echo "> Fichier lu depuis le répertoire dir_b avec succès";

sudo -u lambda_b bash -c 'cat dir_c/admin1.txt > /dev/null' \
&& echo "> Fichier lu depuis le répertoire dir_c avec succès";

sudo -u lambda_b bash -c 'rm -f dir_c/admin1.txt 2> /dev/null' \
|| echo "> Suppression de fichier dans le repertoire dir_c refusé";

sudo -u lambda_b bash -c 'mv dir_c/admin1.txt dir_c/lambda_b1.txt 2> /dev/null' \
|| echo "> Renommage de fichier dans le repertoire dir_c refusé";

sudo -u lambda_b bash -c '(echo "Should fail" >> dir_c/admin1.txt) 2> /dev/null' \
|| echo "> Modification de fichier dans le repertoire dir_c refusé";

sudo -u lambda_b bash -c 'touch dir_c/file.txt 2> /dev/null' \
|| echo "> Création de fichier dans le repertoire dir_c refusé";

sudo -u lambda_b bash -c 'echo "Should work" >> dir_b/admin1.txt' \
|| echo "> Fonctionne pas (Fichier modifié dans le répertoire dir_b avec succès)";

sudo -u lambda_b bash -c 'touch dir_b/lambda_b1.txt' \
&& echo "> Fichier créé dans le répertoire dir_b avec succès";

sudo -u lambda_b bash -c 'mkdir dir_b/lambda_b' \
&& echo "> Repertoire créé dans le répertoire dir_b avec succès";

sudo -u lambda_b bash -c 'rm -f dir_b/admin1.txt 2> /dev/null' \
|| echo "> Suppression de fichier, ne lui appartenant pas, dans le repertoire dir_b refusé";

sudo -u lambda_b bash -c 'mv dir_b/admin1.txt dir_b/lambda_b2.txt 2> /dev/null' \
|| echo "> Renommage de fichier, ne lui appartenant pas, dans le repertoire dir_b refusé";

sudo -u lambda_b bash -c 'ls -al dir_a 2> /dev/null' || echo "> Tout accès au repertoire dir_b refusé";
