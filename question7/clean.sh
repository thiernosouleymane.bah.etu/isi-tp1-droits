#!/bin/bash

# Delete files
sudo rm -rf dir_*

# Remove users

sudo deluser lambda_a
sudo deluser lambda_b
sudo deluser admin


# Remove groups
sudo delgroup groupe_a
sudo delgroup groupe_b
sudo delgroup admin