#!/bin/bash

### Test de l'utilisateur admin
# Il peut modifier, créer et renommer des fichiers dans dir_a, dir_b et dir_c
# Il peut lire tous les fichiers dans dir_a, dir_b et dir_c
# Il peut supprimer des fichiers dans dir_a, dir_b et dir_c

sudo -u admin bash -c 'echo "Lorem ipsum dolor sit amet, consectetur adipiscing elit." > dir_a/admin1.txt' \
&& echo "> Fichier créé et édité dans le répertoire dir_a avec succès";

sudo -u admin bash -c 'echo "Lorem ipsum dolor sit amet, consectetur adipiscing elit." > dir_b/admin1.txt' \
&& echo "> Fichier créé et édité dans le répertoire dir_b avec succès";

sudo -u admin bash -c 'echo "Lorem ipsum dolor sit amet, consectetur adipiscing elit." > dir_c/admin1.txt' \
&& echo "> Fichier créé et édité dans le répertoire dir_c avec succès";

sudo -u admin bash -c 'cat dir_a/admin1.txt > /dev/null' \
&& echo "> Fichier lu depuis le répertoire dir_a avec succès";

sudo -u admin bash -c 'cat dir_b/admin1.txt > /dev/null' \
&& echo "> Fichier lu depuis le répertoire dir_b avec succès";

sudo -u admin bash -c 'cat dir_c/admin1.txt > /dev/null' \
&& echo "> Fichier lu depuis le répertoire dir_c avec succès";

sudo -u admin bash -c 'touch dir_a/admin2.txt dir_b/admin2.txt dir_c/admin2.txt' \
&& sudo -u admin bash -c 'rm dir_a/admin2.txt dir_b/admin2.txt dir_c/admin2.txt' \
&& echo "> Fichier supprimé des répertoires dir_a, dir_b et dir_c avec succès";
