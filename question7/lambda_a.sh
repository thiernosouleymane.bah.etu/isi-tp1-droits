#!/bin/bash

### Test de l'utilisateur lambda_a

sudo -u lambda_a bash -c 'cat dir_a/admin1.txt > /dev/null' \
&& echo "> Fichier lu depuis le répertoire dir_a avec succès";

sudo -u lambda_a bash -c 'cat dir_c/admin1.txt > /dev/null' \
&& echo "> Fichier lu depuis le répertoire dir_c avec succès";

sudo -u lambda_a bash -c 'rm -f dir_c/admin1.txt 2> /dev/null' \
|| echo "> Suppression de fichier dans le repertoire dir_c refusé";

sudo -u lambda_a bash -c 'mv dir_c/admin1.txt dir_c/lambda_a1.txt 2> /dev/null' \
|| echo "> Renommage de fichier dans le repertoire dir_c refusé";

sudo -u lambda_a bash -c '(echo "Should fail" >> dir_c/admin1.txt) 2> /dev/null' \
|| echo "> Modification de fichier dans le repertoire dir_c refusé";

sudo -u lambda_a bash -c 'touch dir_c/file.txt 2> /dev/null' \
|| echo "> Création de fichier dans le repertoire dir_c refusé";

sudo -u lambda_a bash -c 'echo "Should work" >> dir_a/admin1.txt' \
|| echo "> Fonctionne pas (Fichier modifié dans le répertoire dir_a avec succès)";

sudo -u lambda_a bash -c 'touch dir_a/lambda_a1.txt' \
&& echo "> Fichier créé dans le répertoire dir_a avec succès";

sudo -u lambda_a bash -c 'mkdir dir_a/lambda_a' \
&& echo "> Repertoire créé dans le répertoire dir_a avec succès";

sudo -u lambda_a bash -c 'rm -f dir_a/admin1.txt 2> /dev/null' \
|| echo "> Suppression de fichier, ne lui appartenant pas, dans le repertoire dir_a refusé";

sudo -u lambda_a bash -c 'mv dir_a/admin1.txt dir_a/lambda_a2.txt 2> /dev/null' \
|| echo "> Renommage de fichier, ne lui appartenant pas, dans le repertoire dir_a refusé";

sudo -u lambda_a bash -c 'ls -al dir_b 2> /dev/null' || echo "> Tout accès au repertoire dir_b refusé";
